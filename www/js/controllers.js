angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $rootScope,$cordovaOauth,$http,$cordovaToast) {

  $scope.list = ["first","second"];

  //By default we aren't logged in
	$scope.noAuth = true;
	$scope.selectedImage = "";
	$scope.status = {message:""};

	//temp
	var clientId = "vtee1jq3b0ju8k8b3aet4jdaqmd2x542";
	var clientSecret = "AFu65NmociT2a3YpWKLwZUxkqnXcLPUE";
	var state = "";

	$scope.doAuth = function() {
		$cordovaToast.showLongBottom('Beginning to auth against Box').then(function(success) {
	    // success
	  }, function (error) {
	    // error
	  });

		$cordovaOauth.box(clientId, clientSecret,state).then(function(result) {
			$cordovaToast.showLongBottom('Successful log to Box').then(function(success) {
		    // success
		  }, function (error) {
		    // error
		  });
			token = result.access_token;
			$scope.noAuth = false;
		}, function(error) {
			console.log('Error',error);
		});
	}

	$scope.doPicture = function() {

		navigator.camera.getPicture(function(uri) {
			$scope.selectedImage = uri;
			$scope.status.message = "Uploading bits to Box...";
			$scope.$apply();

			$cordovaToast.showLongBottom('Going to send a file to Box').then(function(success) {
		    // success
		  }, function (error) {
		    // error
		  });

			var win = function (r) {
			    console.log("Code = " + r.responseCode);
			    console.log("Response = " + r.response);
			    console.log("Sent = " + r.bytesSent);
				$scope.status.message = "Sent to box!";
				$cordovaToast.showLongBottom('Sent a file to box!').then(function(success) {
			    // success
			  }, function (error) {
			    // error
			  });
				$scope.$apply();
			}

			var fail = function (error) {
			    alert("An error has occurred: Code = " + error.code);
			    console.log("upload error source " + error.source);
			    console.log("upload error target " + error.target);
				$cordovaToast.showLongBottom('Failed to send to Box').then(function(success) {
			    // success
			  }, function (error) {
			    // error
			  });
				console.dir(error);
			}

			var options = new FileUploadOptions();
			options.fileKey = "file";
			options.fileName = uri.substr(uri.lastIndexOf('/') + 1);
			options.mimeType = "image/jpeg";

			var headers={'Authorization':'Bearer '+token};

			options.headers = headers;

			var params = {};
			params.attributes = '{"name":"'+options.fileName+'", "parent":{"id":"0"}}';

			options.params = params;
			console.dir(options);
			var ft = new FileTransfer();
			ft.upload(uri, encodeURI("https://upload.box.com/api/2.0/files/content"), win, fail, options);



		}, function(err) {
			console.log("Camera error", err);
		}, {
			quality:25,
			destinationType:Camera.DestinationType.FILE_URI,
			sourceType:Camera.PictureSourceType.PHOTOLIBRARY
		});
  }


})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
